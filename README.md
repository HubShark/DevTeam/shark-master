# SharkMaster
is almost a revival of Remastersys 2.0.24-1 and modded to fit our needs.

## What can it do?
at this point in development, I'm not sure. Plans are
  - to remove the backup stuff.
    - I believe there are tools that do backups better.
  - tidy up the dialog based installer
  - maybe remove the gui stuff.
    - not really needed as a developer
  - at some point make installer multi-language

## I need a GUI Installer
Don't worry. I have grabbed [Fervi's Installer](https://github.com/fervi/live-installer-remastersys) and modded that to fit our needs.

The tool has been renamed to [sm-live-installer](https://gitlab.com/HubShark/DevTeam/sm-live-installer). *NOTE: this link takes you to our git repo**

## Documentation
At the time the documentation will be on the [WIKI](wikis/home).

Later, as the project develops, we will have documetion available on the [Main Site](http//hubshark.com).
